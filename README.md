# Dependencies

- [iOS App Signer](https://dantheman827.github.io/ios-app-signer/)
- [Cydia Impactor](http://www.cydiaimpactor.com/)

# Before using

1. Open **iOS App Signer** and make sure that the correct *Signing Certificate* is selected.

2. Open **Cydia Impactor** and make sure that the correct device is selected.

You can close both apps once that is done.

# Usage

1. Open the macOS Terminal and execute the following command:
```
$ ./main.sh /PATH/TO/IPA/FILES
```

Make sure to put the path to the folder that contains the IPA files.

2. Enter your Apple ID (usually the email associated to your developer account) and your password (it is **recommended** to use [app-specific passwords](https://support.apple.com/en-us/HT204397)).

If you don't want to enter the credentials everytime, you can also use environment variables as following:
```
export IMPACTOR_APPLEID_PASSWORD="your@email.com"
export IMPACTOR_APPLEID_PASSWORD="your-password"
```
Make sure to put your correct email and password.

# Authors

- [Thibault Ehrhart](https://github.com/thibaultehrhart)
