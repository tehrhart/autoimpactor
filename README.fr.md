# Dépendances

- [iOS App Signer](https://dantheman827.github.io/ios-app-signer/)
- [Cydia Impactor](http://www.cydiaimpactor.com/)

# Avant d'utiliser

1. Ouvrir **iOS App Signer** et vérifier que le bon *Signing Certificate* est sélectionné.

2. Ouvrir **Cydia Impactor** et vérifier que le bon appareil est sélectionné.

Vous pouvez ensuite fermer ces applications.

# Utilisation

1. Ouvrir le Terminal de macOS et exécuter la commande suivante :
```
$ ./main.sh /CHEMIN/VERS/LES/FICHIERS/IPA
```

Assurez-vous d'indiquer le chemin vers le dossier qui contient les fichiers IPA.

2. Entrez votre Apple ID (généralement l'email associé à votre compte développeur) et votre mot de passe (il est **recommandé** d'utiliser un [mot de passe pour application](https://support.apple.com/fr-fr/HT204397)).

Si vous ne souhaitez pas entrer vos identifiants à chaque fois, vous pouvez également utiliser des variables d'environnement comme indiqué ci-dessous:
```
export IMPACTOR_APPLEID_PASSWORD="votre@email.com"
export IMPACTOR_APPLEID_PASSWORD="votre-mot-de-passe"
```
Assurez-vous d'indiquer les bons identifiants.

# Auteurs

- [Thibault Ehrhart](https://github.com/thibaultehrhart)
